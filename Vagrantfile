# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
module OS
    # Try detecting Windows
    def OS.windows?
        (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
    end
end

Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/bionic64"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.35.10"

  # $USER and $GROUPS are set when a user logs in. -- in Ubuntu Bionic at least
  # %USERNAME% is in Windows 10.
  unless ENV['KOHACLONE_DIR']
    print "Defaulting KOHACLONE_DIR\n"
    if OS.windows?
      ENV['KOHACLONE_DIR'] = "C:\\user\\#{ENV['USERNAME']}\\Documents\\kohaclone"
    else
      ENV['KOHACLONE_DIR']="/home/#{ENV['USER']}/kohaclone"
    end
  end
  print "Using (", ENV['KOHACLONE_DIR'], ") as the Koha path.\n"
  unless ENV['DEBIAN_DIR']
    print "Defaulting DEBIAN_DIR\n"
    if OS.windows?
      ENV['DEBIAN_DIR'] = "C:\\user\\#{ENV['USERNAME']}\\Documents\\debian"
    else
      ENV['DEBIAN_DIR']="/home/#{ENV['USER']}/debian"
    end
  end
  print "Using (", ENV['DEBIAN_DIR'], ") as the Output path.\n"
  unless ENV['GIT_BRANCH']
    print "Defaulting GIT_BRANCH\n"
    ENV['GIT_BRANCH']="19.05"
  end
  print "Using (", ENV['GIT_BRANCH'], ") as the branch to build.\n"
  #ENV['DOCKER_MOUNT_USERNAME']=`id -u -n | tr -d '\n'`
  #ENV['DOCKER_MOUNT_GROUPNAME']=`id -g -n| tr -d '\n'`
  #print "Using (" + ENV['DOCKER_MOUNT_USERNAME'] + ":" + ENV['DOCKER_MOUNT_GROUPNAME'] + ") as the owner:group permissions.\n"
  config.vm.synced_folder ENV['KOHACLONE_DIR'], "/koha", create: true
  config.vm.synced_folder ENV['DEBIAN_DIR'], "/debs", create: true
  #  owner: ENV['DOCKER_MOUNT_USERNAME'], group: ENV['DOCKER_MOUNT_GROUPNAME']

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
     # Customize the amount of memory on the VM:
     vb.memory = "2048"

     # A fix for speed issues with DNS resolution:
     # http://serverfault.com/questions/453185/vagrant-virtualbox-dns-10-0-2-3-not-working?rq=1
     vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # This provisioner runs on the first `vagrant up`.
  config.vm.provision "install", type: "shell", inline: <<-SHELL
    # Know what to do with the docker containers
    # This is the first time, so it isn't default "web"
    export WHICH_ONE="start"
    # update repo
    sudo apt-get update
    # Allow install over HTTPS
    sudo apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
    # Allow cgroup use in pbuilder
    sudo apt-get install -y dbus-user-session

    # Add Docker's official GPG key
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    # Add the stable repository
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    # re-update repos
    sudo apt-get update
    # install the latest
    sudo apt-get install -y docker-ce docker-ce-cli containerd.io
    # Make sure the vagrant account has permissions
    sudo usermod -a -G docker vagrant

    # Set up kohaclone and debian directories
    # Building inside the virtual box doesn't like mount'd mounts.
    sudo mkdir /home/vagrant/debian
    sudo chown vagrant.vagrant /home/vagrant/debian
    sudo cp -R -v /koha /home/vagrant
    sudo mv /home/vagrant/koha /home/vagrant/kohaclone
    sudo chown vagrant.vagrant -R /home/vagrant/kohaclone
 
    echo "# Environment variables built by created vagrant" > /vagrant/common.env
    echo >> /vagrant/common.env
    echo "export KOHACLONE_DIR=/home/vagrant/kohaclone" >> /vagrant/common.env
    echo "export DEBIAN_DIR=/home/vagrant/debian" >> /vagrant/common.env
    echo "export GIT_BRANCH=#{ENV['GIT_BRANCH']}" >> /vagrant/common.env

    # make it as if make clean, make configure has been run
    cd /vagrant
    ./clean_dockery.sh
    ./clean_debs.sh

  SHELL

  # This provisioner runs on every `vagrant reload' (as well as the first
  # `vagrant up`), reinstalling from local directories
  config.vm.provision "recompose", type: "shell",
     run: "always", inline: <<-SHELL

    # Run docker-compose (which will update preloaded images, and
    # pulls any images not preloaded)
    cd /vagrant
    
    # Set necessary environment variables for shell access.
    export COMPOSER_CACHE_DIR=/tmp
    export DOCKER_UIDGID="$(id -u):$(id -g)"

    # Set up necessary env. vars to automatically be present each time.
    cat << 'EOF' >> /home/vagrant/.bashrc
export COMPOSER_CACHE_DIR=/tmp
export DOCKER_UIDGID="$(id -u):$(id -g)"
EOF

  SHELL

end
