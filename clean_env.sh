#!/bin/sh

if [ -e "common.env" ]; then
    rm -f common.env
else
    echo "There is no common environment file."
fi
