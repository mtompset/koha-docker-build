configure:
	./configure.sh

image:
	./build_image.sh

build:
	./run_builder.sh

clean:
	@./clean_debs.sh
	@./clean_env.sh
	@./clean_dockery.sh
