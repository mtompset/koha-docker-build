#!/bin/sh

echo "This configures what branch and patches will be used in your"
echo "custom build"
echo
echo "The koha git source directory is usually /home/$USER/kohaclone"

KOHACLONE_DIR=""
while [ ! -d "$KOHACLONE_DIR" ]
do
    if [ "X$KOHACLONE_DIR" != "X" ]; then
        echo "$KOHACLONE_DIR does not exist."
    fi
    read -p "Enter where your koha git source directory is: " KOHACLONE_DIR
done
echo "Using ($KOHACLONE_DIR) as the koha git source directory."

if [ ! -d "$KOHACLONE_DIR/.git" ]; then
    echo "WARNING: It is not a git directory!"
    echo "Please make sure to:"
    echo "    git clone git://git.koha-community.org/koha.git $KOHACLONE_DIR"
    echo "before attempting to build anything."
fi

echo
echo "The debian build directory is usually /home/$USER/debian"
DEBIAN_DIR=""
while [ ! -d "$DEBIAN_DIR" ]
do
    if [ "X$DEBIAN_DIR" != "X" ]; then
        echo "$DEBIAN_DIR does not exist."
    fi
    read -p "Enter where your debian build directory is: " DEBIAN_DIR
done
echo "Using ($DEBIAN_DIR) as the debian build directory."

echo "These are the available branches, don't type the .x!"
OLD_DIR=`pwd`
cd $KOHACLONE_DIR
git branch -a | grep remote | sort -r | grep "[.]x" | grep -v "\/3[.]" | cut -f3 -d'/'
cd $OLD_DIR
GIT_BRANCH=""
while [ -z "$GIT_BRANCH" ]
do
    read -p "Enter the branch you wish to build: " GIT_BRANCH
done
echo "Going to attempt to build ${GIT_BRANCH}"

echo "# Environment variables built using ./configure.sh" > common.env
echo >> common.env
echo "export KOHACLONE_DIR=${KOHACLONE_DIR}" >> common.env
echo "export DEBIAN_DIR=${DEBIAN_DIR}" >> common.env
echo "export GIT_BRANCH=${GIT_BRANCH}" >> common.env
