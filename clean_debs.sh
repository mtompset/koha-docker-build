#!/bin/sh

if [ -f "./common.env" ]; then
    . ./common.env
else
    echo "Unable to determine DEBIAN_DIR to clean."
    exit 0;
fi
if [ ! -d ${DEBIAN_DIR} ]; then
    echo "${DEBIAN_DIR} doesn't exist... Skipping."
    exit 0
fi
CHECK=`ls ${DEBIAN_DIR}`
if [ -z "$CHECK" ]; then
    echo There are no output files to clean up.
else
    rm -f ${DEBIAN_DIR}/*
fi
