#!/bin/sh

if [ ! -f common.env ]; then
    echo "Please run 'make configure' and 'make image' first."
    exit 1
fi
. ./common.env
echo "Attempting to trigger the image to build ${GIT_BRANCH} from the ${KOHACLONE_DIR} into the ${DEBIAN_DIR}..."
sudo docker run --privileged -it --env GIT_BRANCH=${GIT_BRANCH} --volume ${DEBIAN_DIR}:/debs --volume ${KOHACLONE_DIR}:/koha -h kohabuilder --name koha_building koha_builder

if [ "${DEBIAN_DIR}" = "/home/vagrant/debian" ]; then
    cp -R -v ${DEBIAN_DIR}/* /debs
    echo "Debian packages moved as needed!"
else
    echo "Debian packages built in place!"
fi
echo "Build Completed!"
