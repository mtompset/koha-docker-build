#!/bin/sh

if [ -f common.env ]; then
    . ./common.env
fi
vagrant destroy
rm common.env
