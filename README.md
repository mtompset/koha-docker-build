# Koha Docker Builder #

## Description ##

This is intended to build the Debian package files from within
Docker containers.

## Required Development Tools ##

1. Git
   - Windows users: download from https://git-scm.com/download/win
     Be sure to disable `autocrlf` in Git, 'as-is' is the recommended setting.
   - Debian users: sudo apt-get install git git-email
2. Virtual Box
   - Windows users: download from
     https://www.virtualbox.org/wiki/Downloads
   - Debian users: download from
     https://www.virtualbox.org/wiki/Linux_Downloads
3. Vagrant Up
   - Download the appropriate version from:
     https://www.vagrantup.com/downloads.html

## Setting up local dev environment ##

Having a local kohaclone directory is useful:
* git clone git://git.koha-community.org/koha.git kohaclone

This is over 3GB, so it may take a while.
KOHACLONE_DIR should point to this directory


A directory to place the debian output files is needed:
* mkdir ~/debian

This is /home/mtompset/debian for mtompset, but it will be
/home/vagrant/debian for the vagrant environment.
DEBIAN_DIR should point to this directory.


In a Linux environment, this should be runnable from the command-line

0. make clean
   - remove all the docker images, containers, and common.env file
   - It should be as if nothing had been run

1. make configure
   - This will trigger a simple step by step set of questions to create
     the contents of common.env
   - The file common.env should have three environment variables exported:
     1. KOHACLONE_DIR -- The full path to the koha source code
     2. DEBIAN_DIR    -- The full path to where the package files will be built
     3. GIT_BRANCH    -- The branch which will be built
   - Takes less than a minute to set up!

2. make image
   - This will be the image used to generate the docker image that once instantiated
     as a container will build the package files
   - IT MAY TAKE A LONG TIME (6+ minutes)

3. make build
   - This runs the image.
   - IT MAY TAKE A LONG TIME (25+ minutes)
   - The package files will be created in the DEBIAN_DIR

In a non-Linux environment, you may need to run VirtualBox.
If you are not running on Ubuntu Bionic and have issues attempt
this method.

1. make clean

2. KOHACLONE_DIR={path to kohaclone dir} DEBIAN_DIR={path to debian output dir} ./vup.sh
   -- this should download and set up an Ubunu Bionic VM
   -- it currently has /home/mtompset/kohaclone and
      /home/mtompset/debian hardcoded
   -- they will be mounted as /koha and /debs respectively
   -- using the environment variables before the script should provide
      other paths to use.

3. ./vssh.sh
   -- this should mount and ssh into the Ubuntu Bionic VM

4. cd /vagrant
   -- this will contain the same scripts and files here.
   *** KNOWN BUG: edit the common.env to make sure it has the branch ***

5. make image
   -- this image is a Debian Stretch docker image

6. make build
   -- the output will be in /home/vagrant/debian
   -- the kohaclone directory was set up /home/vagrant/kohaclone
   -- to pass the debs back to your host machine, you can copy
      them from /home/vagrant/debian to /debs
   -- this intermediate set bypasses permission issues of mounting
      and mount'd directory between multiple levels of virtualization.
