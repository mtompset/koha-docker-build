#!/bin/sh

# This is run on the host, which triggers this script.
# sudo ln -s /home/mtompset/debian /debs
# sudo ln -s /home/mtompset/kohaclone /koha
# docker create --env GIT_BRANCH=18.11 --volume /debs:/debs --volume /koha:/koha --name koha_built koha_builder

export GIT_DISCOVERY_ACROSS_FILESYSTEM=1
if [ -z "${GIT_BRANCH}" ]; then
    echo "Missing GIT_BRANCH environment variable!"
    exit 1;
fi
echo "#!/bin/sh" | sudo tee initialize_base.sh
if [ "${GIT_BRANCH}" = "master" ]; then
    cd /debs
    GIT_BRANCH_NAME=master
    VERSION_OPTIONS="--autoversion"
    echo "echo \"deb http://debian.koha-community.org/koha unstable main\" | tee -a /etc/apt/sources.list.d/koha_autobuild.list" | sudo tee -a initialize_base.sh
else
    cd /debs
    GIT_BRANCH_NAME=${GIT_BRANCH}.x
    VERSION_OPTIONS="-v ${GIT_BRANCH_NAME} --autoversion"
    echo "echo \"deb http://debian.koha-community.org/koha ${GIT_BRANCH} main\" | tee -a /etc/apt/sources.list.d/koha_autobuild.list" | sudo tee -a initialize_base.sh
fi

echo 'apt-get install -y wget gnupg2
wget -O- http://debian.koha-community.org/koha/gpg.asc | apt-key add -
apt-get update
apt-get install -y libhttp-oai-3.27-perl libmojolicious-plugin-openapi-perl libnet-oauth2-authorizationserver-perl libpdf-fromhtml-perl libtemplate-plugin-htmltotext-perl libwebservice-ils-perl
apt-get install -y devscripts pbuilder dh-make fakeroot debian-archive-keyring koha-perldeps koha-deps koha-elasticsearch libtest-simple-perl liburi-perl libmodule-build-tiny-perl dh-make-perl
apt-get install -y libcache-perl xsltproc docbook-xsl libxml2-utils libtest-simple-perl cgroup*

echo 127.0.0.1	`hostname` | tee -a /etc/hosts
export GIT_DISCOVERY_ACROSS_FILESYSTEM=1' | sudo tee -a initialize_base.sh

chmod 755 initialize_base.sh

echo Creating base distribution build environment...
sudo pbuilder create --distribution stretch --mirror ftp://ftp.ca.debian.org/debian/

echo Initialize base distribution for the build...
sudo pbuilder execute --save-after-exec -- /debs/initialize_base.sh

cd /koha
git checkout master
CHECK_BRANCH=`git branch -a | grep "my_${GIT_BRANCH_NAME}" | wc -l`
if [ "${CHECK_BRANCH}" = "1" ]; then
    git branch -D my_${GIT_BRANCH_NAME}
fi

# To make sure there's no noise.
git checkout origin/master -- debian/changelog

git checkout -b my_${GIT_BRANCH_NAME} origin/${GIT_BRANCH_NAME}
DEB_BUILD_OPTIONS=nocheck ./debian/build-git-snapshot -r /debs ${VERSION_OPTIONS} -d
