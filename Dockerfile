FROM debian:stretch
ENV REFRESHED_AT 2019-07-19
LABEL maintainer="mtompset@hotmail.com"

# used to checkout a branch to build
# docker build --build-arg GIT_BRANCH_TAG=18.11 -t koha_builder .
ARG GIT_BRANCH_TAG

# Seemed good to copy from tcohen's version
ENV PATH /usr/bin:/bin:/usr/sbin:/sbin
ENV DEBIAN_FRONTEND noninteractive


# Make sure it doesn't use root
#USER $UID:$GROUPS

# This is passed as --volume=/path/to/your/clone:/koha in the docker create
VOLUME /koha
# This is passed as --volume=/path/to/put/debs:/debs in the docker create
VOLUME /debs

RUN apt-get update -y
RUN apt-get upgrade -y 
RUN apt-get install -q -y wget apt-utils gnupg2 libterm-readline-perl-perl

RUN echo "deb http://debian.koha-community.org/koha ${GIT_BRANCH_TAG} main" > /etc/apt/sources.list.d/koha.list
RUN wget -O- http://debian.koha-community.org/koha/gpg.asc | apt-key add -

RUN apt-get -y update

RUN apt-get -y --allow-unauthenticated install \
      devscripts \
      pbuilder \
      dh-make \
      fakeroot \
      debian-archive-keyring \
      build-essential \
      git \
      git-email \
      libmodern-perl-perl \
      koha-perldeps \
      koha-deps \
      libcache-perl \
      xsltproc \
      docbook-xsl \
      libxml2-utils \
      cgroup* \
      bash-completion \
      libtest-simple-perl \
   && rm -rf /var/cache/apt/archives/*

# To enable C4 and Koha perl libraries to be found
ENV PERL5LIB=/koha
ENV GIT_BRANCH=${GIT_BRANCH_TAG}

# This will actually do all the checkouts and building on the docker container
ADD custom_roll.sh /custom_roll.sh
CMD /custom_roll.sh
