#!/bin/sh

CHECK=`which docker 2>/dev/null | grep "docker" | wc -l`
if [ "$CHECK" -eq "0" ]; then
    echo "Docker isn't even installed! Nothing dockery to clean up."
    exit 0
fi

CHECK=`docker image ls -a | grep koha_builder`
if [ ! -z "$CHECK" ]; then
    docker rmi --force koha_builder > /dev/null
    echo "Docker image removed."
else
    echo "There is no koha_builder image."
fi

CHECK=`docker container ls -a | grep koha_building`
if [ ! -z "$CHECK" ]; then
    docker container rm --force koha_building > /dev/null
    echo "Docker container removed."
else
    echo "There is no koha_builder container."
fi
