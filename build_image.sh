#!/bin/sh

if [ ! -f common.env ]; then
    echo "Please run 'make configure' first."
    exit 1
fi

. ./common.env # sh equivalent of bash's source ./common.env
echo "Attempting to build ${GIT_BRANCH} image..."
docker build --build-arg GIT_BRANCH_TAG=${GIT_BRANCH} -t koha_builder .
